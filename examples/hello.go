package main

import "fmt"

func action() (i int) {
	i = 0
	defer fmt.Println("defer(1):", i)
	i++
	defer fmt.Println("defer(2):", i)
	i++
	defer fmt.Println("defer(3):", i)
	i++
	defer func() {
		i++
		fmt.Println("change return value from", i-1, "to", i)
	}()
	fmt.Println("no defer, before return statement, return value =", i)
	return i
}

func main() {
	res := action()
	fmt.Println(res)
}
