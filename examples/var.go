package varTest

var a, b, c = 1, 2, 3*4 + 1
var (
	q, w, e = 1, 2, 3
	y, u, i = 4, 5, 6
)

func main() {
	println(a, b, c, q, w, e, y, u, i)
	switch yyy := a + b; yyy {
	case 3:
		println("case 1")
	case 4:
		println("case 2")
	}
	switch {
	case a+b == 1:
		println("==1")
	default:
		println("!=1")
	}
}
