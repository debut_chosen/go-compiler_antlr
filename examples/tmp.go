package tmp

import (
	"fmt"
	"math"
)

type Point struct {
	X float64 "tag = "
	Y float64 "tagy"
}

type Path []Point

// 函数
func Distance(p, q Point) float64 {
	return math.Hypot(q.X-p.X, q.Y-p.Y)
}

// Point 类型的方法
func (p Point) Distance(q Point) float64 {
	return math.Hypot(q.X-p.X, q.Y-p.Y)
}

// Path 类型的方法
func (path Path) Distance() float64 {
	sum := 0.0
	for i := range path {
		if i > 0 {
			sum += path[i-1].Distance(path[i])
		}
	}
	return sum
}

func main() {
	fmt.Println("Hello world")
	p := Point{1, 2}
	q := Point{4, 6}
	perim := Path{
		{1, 1},
		{5, 1},
		{5, 4},
		{1, 1},
	}
	fmt.Println(p, q, perim)
}
