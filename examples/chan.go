package main

import "fmt"

func proc(x int, c chan int) {
	fmt.Printf("x: %d, c: %d\n", x, c)
	c <- x
}

func main() {
	var c1, c2, c3 chan int
	var i1, i2 int

	i2, i1 = 1, 1
	go proc(i1, c1)
	go proc(i2, c3)

	select {
	case i1 = <-c1:
		fmt.Printf("received ", i1, " from c1\n")
	case c2 <- i2:
		fmt.Printf("sent ", i2, " to c2\n")
	case i3, ok := (<-c3): // same as: i3, ok := <-c3
		if ok {
			fmt.Printf("received ", i3, " from c3\n")
		} else {
			fmt.Printf("c3 is closed\n")
		}
	default:
		fmt.Printf("no communication\n")
	}
}
