import java.io.IOException;

import org.antlr.v4.runtime.*;
import org.antlr.v4.runtime.tree.*;

public class app {
    public static void main(String[] args) {
        String fileName = "./examples/exprTest1.go";
        GoParser parser = null;
        try{
            CharStream stream = CharStreams.fromFileName(fileName);
            GoLexer lexer = new GoLexer(stream);
            CommonTokenStream tokens = new CommonTokenStream(lexer);
            parser = new GoParser(tokens);
            
        } catch(IOException e){
            e.printStackTrace();
        }

        ParseTree ast = parser.sourceFile();
        VisitHandler_index visitor = new VisitHandler_index();
        visitor.visit(ast);
        
        // ParseTreeWalker walker = new ParseTreeWalker();
        // ExpressionListenerHandler listener = new ExpressionListenerHandler();
        // walker.walk(listener, ast);
    }
}
