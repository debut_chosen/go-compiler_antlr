package models;
import java.lang.reflect.Array;
import java.util.ArrayList;

public class VN {
    protected static int index;
    protected static String symbol = "E";
    protected ArrayList<String> codes = new ArrayList<>();
    public ArrayList<String> gen(){
        return null;
    };
    public String newTemp(){
        return (symbol + "_" + index++);
    }
}

/*
全局静态属性：
    static int，存放现在的角标


一个非终结符的属性：code, place
    code 表示求该值的三地址代码
        现目前的三地址代码语句集合 ArrayList<String> codes
    place用来存放该终结符的名字

函数: newTemp() 用来生成不同角标的名字，
    实现方式：在NTSymbol中维护一个static int，存放现在的角标

函数：gen() 用来生成三地址代码语句
    用来往现有的三地址代码语句记录中添加新的语句
    返回值为String类型



*/
