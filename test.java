import java.security.SecureClassLoader;

public class test {

    public class People{
        public String name;
        public String id;
        People(String name, String id){
            this.name = name;
            this.id = id;
        }
    }

    public class Student extends People{
        public String std_id;
        public int rank;
        Student(String name, String id, String std_id, int rank){
            super(name, id);
            this.std_id = std_id;
            this.rank = rank;
        }
    }
}
